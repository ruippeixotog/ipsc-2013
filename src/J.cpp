#include <iostream>
#include <string>
#include <queue>
#include <vector>
#include <algorithm>

using namespace std;

#define N_INPUTS 3
#define INPUTS (1 << N_INPUTS)
#define INPUT_ARRANGEMENTS (N_INPUTS * N_INPUTS * N_INPUTS)
#define MAX_GATES (1 << INPUTS)

bool isUniversal[MAX_GATES];
bool visited[MAX_GATES];

string inBinary(int truthTable) {
    string s = "";
    for (int i = 0; i < 8; i++) {
        s += (truthTable & (1 << i) ? '1' : '0');
        if (i + 1 < 8)
            s += ' ';
    }
    return s;
}

vector<int> rearrangeInputs(int truthTable) {
    vector<int> gs;
    for (int i = 0; i < INPUT_ARRANGEMENTS; i++) {
        vector<int> connections;
        for (int j = 0, k = i; j < N_INPUTS; j++, k /= N_INPUTS)
            connections.push_back(k % N_INPUTS);
        int newTruthTable = 0;
        for (int j = 0; j < INPUTS; j++) {
            int input = 0;
            for (int k = 0; k < N_INPUTS; k++) {
                input |= ((j >> connections[k] & 1) << k);
            }
            newTruthTable |= (truthTable >> input & 1) << j;
        }
        gs.push_back(newTruthTable);
    }
    return gs;
}

int combine(vector<int> inputTruthTables, int truthTable2) {
    int current = 0;
    for (int i = 0; i < INPUTS; i++) {
        int inputToT2 = 0;
        for (int j = 0; j < N_INPUTS; j++) {
            int input = 0;
            for (int k = 0; k < N_INPUTS; k++) {
                input |= (i >> k & 1) << k;
            }
            inputToT2 |= (inputTruthTables[j] >> input & 1) << j;
        }
        current |= (truthTable2 >> inputToT2 & 1) << i;
    }
    return current;
}

void calculateUniversalGates() {
    memset(isUniversal, false, sizeof(isUniversal));

    // This is slow as hell, but eventually outputs all universal gates.
    for (int t = 0; t < MAX_GATES; t++) {
        memset(visited, false, sizeof(visited));
        visited[t] = true;
        vector<int> truthTables;
        truthTables.push_back(t);
        vector<int> ng = rearrangeInputs(t);
        for (size_t i = 0; i < ng.size(); i++) {
            if (!visited[ng[i]])
                truthTables.push_back(ng[i]);
            visited[ng[i]] = true;
        }

        for (int i = 0; i < MAX_GATES; i++) {
            if (i >= (int) truthTables.size())
                break;
            for (int j = 0; j <= i; j++) {
                for (int k = 0; k <= j; k++) {
                    vector<int> inputs;
                    inputs.push_back(truthTables[i]);
                    inputs.push_back(truthTables[j]);
                    inputs.push_back(truthTables[k]);
                    sort(inputs.begin(), inputs.end());
                    do {
                        int newTruthTable = combine(inputs, t);
                        if (!visited[newTruthTable]) {
                            vector<int> ng = rearrangeInputs(newTruthTable);
                            for (size_t l = 0; l < ng.size(); l++) {
                                if (!visited[ng[l]])
                                    truthTables.push_back(ng[l]);
                                visited[ng[l]] = true;
                            }
                        }
                    } while (next_permutation(inputs.begin(),
                                              inputs.end()));
                }
            }
        }

        int c = 0;
        for (int i = 0; i < MAX_GATES; i++)
            if (visited[i])
                c++;
        if (c == MAX_GATES)
            isUniversal[t] = true;
    }
}

int main() {
    calculateUniversalGates();
    for (int i = 0; i < MAX_GATES; i++) {
        if (isUniversal[i]) {
            cout << inBinary(i) << endl;
        }
    }
    return 0;
}
