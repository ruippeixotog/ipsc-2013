#include <iostream>
#include <cstring>

using namespace std;

#define MOD 1000000009
#define MAX 100001

int T, N;
long long ways[MAX];

long long modAdd(long long a, long long b) {
    return (a + b) % MOD;
}

long long modMult(long long a, long long b) {
    return (a * b) % MOD;
}

void calcWays() {
    int waysUpFromDown[] = {0, 1, 2, 3, 5};

    memset(ways, 0, sizeof(ways));
    ways[0] = 1;
    for (int i = 1; i < MAX; i++)
        for (int j = 1; j <= 4; j++)
            if (i - j >= 0)
                ways[i] = modAdd(ways[i],
                                 modMult(waysUpFromDown[j], ways[i - j]));
}

int main() {
    calcWays();

    cin >> T;
    for (int i = 0; i < T; i++) {
        cin >> N;
        cout << ways[N] << endl;
    }

    return 0;
}
