import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

public class A {
    static PrintStream out;

    public static int[] coins = { 1, 2, 5, 10, 20, 50, 100, 200, 500, 1000,
            2000, 5000, 10000 };

    public static void change(int value) {
        int[] coinCount = new int[coins.length];
        for (int i = coins.length - 1; i >= 0; i--) {
            while (value >= coins[i]) {
                coinCount[i]++;
                value -= coins[i];
            }
        }
        out.print(coinCount[0]);
        for (int i = 1; i < coinCount.length; i++) {
            out.print(" " + coinCount[i]);
        }
        out.println();
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner in = new Scanner(System.in);
        out = new PrintStream("a2.out");

        int t = in.nextInt();
        for (int k = 0; k < t; k++) {
            int e = in.nextInt();
            int c = in.nextInt();
            change(e * 100 + c);
        }
    }
}
