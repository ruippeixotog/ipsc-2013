import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class F1 {
    static Scanner in = new Scanner(System.in);

    public static final int N = 81;
    public static final int K = 50;
    public static final int S = 6;

    public static int lower = 0;
    public static int upper = 81;
    
    public static int div1 = lower + (upper - lower) / 3;
    public static int div2 = lower + (upper - lower) * 2 / 3;

    public static boolean solved = false;

    public static void printRequest() throws FileNotFoundException {
        PrintStream req = new PrintStream("req.out");
        
        System.out.println("Bucket 1 is [" + lower + ", " + div1 + "[");
        System.out.println("Bucket 2 is [" + div1 + ", " + div2 + "[");
        System.out.println("Bucket 3 is [" + div2 + ", " + upper + "[");

        String firstReq = "";
        String secondReq = "";
        for (int i = 0; i < N; i++) {
            if (i < lower || i >= upper) {
                firstReq += '-';
                secondReq += '-';
            } else if (i < div1) {
                firstReq += 'L';
                secondReq += '-';
            } else if (i < div2) {
                firstReq += 'R';
                secondReq += 'L';
            } else {
                firstReq += '-';
                secondReq += 'R';
            }
        }

        req.println("W");
        for (int i = 0; i < K; i++) {
            if (i < K / 2)
                req.println(firstReq);
            else
                req.println(secondReq);
        }
    }

    private static void receiveResponse() {
        String resp = in.nextLine();

        int[] freq = new int[3];
        for (int i = 0; i < K / 2; i++) {
            int code = resp.charAt(i) == 'L' ? 0 : resp.charAt(i) == '=' ? 1 : 2;
            freq[code]++;
        }
        System.out.println(Arrays.toString(freq));
        
        freq[0] = freq[1] = freq[2] = 0;
        for (int i = K / 2; i < K; i++) {
            int code = resp.charAt(i) == 'L' ? 0 : resp.charAt(i) == '=' ? 1 : 2;
            freq[code]++;
        }
        System.out.println(Arrays.toString(freq));
    }

    private static void printCheckRequest(int idx) throws FileNotFoundException {
        PrintStream req = new PrintStream("req.out");
        req.println("W");
        for (int i = 0; i < K; i++) {  
            int randRight = new Random().nextInt(N);
            if(randRight == idx) {
                randRight++;
            }
            for(int j = 0; j < N; j++) {
                if (j == idx)
                    req.print('L');
                else if(j == randRight) {
                    req.print('R');
                } else {
                    req.print('-');
                }
            }
            req.println();
        }
    }
    
    private static void printGuess(int idx, char weight) throws FileNotFoundException {
        PrintStream req = new PrintStream("req.out");
        req.println("G");
        req.println((idx + 1) + " " + weight);
        // TODO Auto-generated method stub
    }

    public static void main(String[] args) throws FileNotFoundException {
        // printCheckRequest(17);
        printGuess(17, 'L');
        System.exit(0);
        
        for (int i = 0; i < S - 1 && !solved; i++) {
            printRequest();
            System.out.println("Written request to file. Give me the response");
            receiveResponse();
            solved = true;
        }
        if (!solved) {
            System.out.println("Fail :(");
        }
        // printGuess(0, 'A');
    }
}
